import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:asincronia_flutter/services/mockapi.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  static const String _title = 'Flutter Code Sample';
  
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Asynchronous Call',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Asynchronous call'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {




  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
    ));
 
    int counter = 0;
    return Scaffold(
      appBar: AppBar(
      
        title: Text(widget.title),     
        centerTitle: true,   
       
      ),
body: AnimatedContainer(
        duration: const Duration(seconds: 1),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children:<Widget> [
              FutureBuilder<int>(
                future: MockApi().getFerrariInteger(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Text(
                      '${snapshot.data}',
                    style: Theme.of(context).textTheme.headline4,
                    );
                  } else if (snapshot.hasError) {
                    return Text(
                      '${snapshot.error}',
                      style: const TextStyle(fontSize: 20),
                    );
                  }
                  return const RefreshProgressIndicator();
                },
              ),
             
              ElevatedButton(
                 style: ElevatedButton.styleFrom(
                backgroundColor: Color.fromARGB(255, 4, 131, 8),
              ),
                onPressed: () async {
                  var result = await MockApi().getFerrariInteger();
                  counter = result;
                  print('Ferrari: $result');
                },  
                child: const Icon(Icons.battery_charging_full_outlined, size: 30, textDirection: TextDirection.ltr, color: Colors.white),
              ),
               FutureBuilder<int>(
                future: MockApi().getHyundaiInteger(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Text(
                      '${snapshot.data}',
                    style: Theme.of(context).textTheme.headline4,
                    );
                  } else if (snapshot.hasError) {
                    return Text(
                      '${snapshot.error}',
                      style: const TextStyle(fontSize: 20),
                    );
                  }
                  return const RefreshProgressIndicator();
                },
              ),
              const SizedBox(height: 20),
              ElevatedButton(
                 style: ElevatedButton.styleFrom(
                backgroundColor: Color.fromARGB(255, 243, 149, 7),
              ),
                onPressed: () async {
                  var result = await MockApi().getHyundaiInteger();
                  setState(() {
                    counter = result;
                    print('Hyundai: $result');
                  });
                },
                child: const Icon(Icons.car_rental, size: 30),
              ),
              const SizedBox(height: 20),
               FutureBuilder<int>(
                future: MockApi().getFisherPriceInteger(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Text(
                      '${snapshot.data}',
                    style: Theme.of(context).textTheme.headline4,
                    );
                  } else if (snapshot.hasError) {
                    return Text(
                      '${snapshot.error}',
                      style: const TextStyle(fontSize: 20),
                    );
                  }
                  return const RefreshProgressIndicator();
                },
              ),
              ElevatedButton(
                 style: ElevatedButton.styleFrom(
                backgroundColor: Color.fromARGB(255, 223, 19, 5),
              ),
                onPressed: () async {
                  var result = await MockApi().getFisherPriceInteger();
                  setState(() {
                    counter = result;
                    print('Fisher Price: $result');
                  });
                },
                //person running
                child: const Icon(Icons.directions_run, size: 30),
              ),
            ],
          ),
        ),
      ),
    );
  }
        
   
}